ROOT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SRC_DIR=$(ROOT_DIR)/src
BIN_DIR=$(ROOT_DIR)/bin

deps:
	@echo "==> Installing dependencies"
	@curl -L https://getcomposer.org/composer.phar -o $(BIN_DIR)/composer
	@COMPOSER_HOME=/tmp $(BIN_DIR)/composer config --global github-protocols https
	@COMPOSER_HOME=/tmp php -d memory_limit=-1 $(BIN_DIR)/composer install

db: deps
	@php $(BIN_DIR)/console --no-ansi --no-interaction --env=dev doctrine:migrations:migrate
	@php $(BIN_DIR)/console --no-ansi --no-interaction --env=dev cache:clear
	@php $(BIN_DIR)/console --no-ansi --no-interaction --env=test doctrine:migrations:migrate
	@php $(BIN_DIR)/console --no-ansi --no-interaction --env=test cache:clear

fixtures: db
	@php $(BIN_DIR)/console --no-ansi --no-interaction --env=test doctrine:fixtures:load

run: db
	@php bin/console server:run 0.0.0.0:8080

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180114090009 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE connection ADD type VARCHAR(255) NOT NULL, ADD name VARCHAR(255) DEFAULT NULL, ADD fio VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD inn VARCHAR(255) NOT NULL, ADD kpp VARCHAR(255) NOT NULL, ADD country VARCHAR(255) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD region VARCHAR(255) NOT NULL, ADD street VARCHAR(255) NOT NULL, ADD building VARCHAR(255) NOT NULL, ADD house VARCHAR(255) DEFAULT NULL, ADD flat VARCHAR(255) DEFAULT NULL, ADD like_cus TINYINT(1) DEFAULT \'1\' NOT NULL, ADD f_country VARCHAR(255) DEFAULT NULL, ADD f_city VARCHAR(255) DEFAULT NULL, ADD f_region VARCHAR(255) DEFAULT NULL, ADD f_street VARCHAR(255) DEFAULT NULL, ADD f_building VARCHAR(255) DEFAULT NULL, ADD f_house VARCHAR(255) DEFAULT NULL, ADD f_flat VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE connection DROP type, DROP fio, DROP name, DROP phone, DROP inn, DROP kpp, DROP country, DROP city, DROP region, DROP street, DROP building, DROP house, DROP flat, DROP like_cus, DROP f_country, DROP f_city, DROP f_region, DROP f_street, DROP f_building, DROP f_house, DROP f_flat');
    }
}

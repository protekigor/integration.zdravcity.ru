<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180120175755 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kit_connection (id INT AUTO_INCREMENT NOT NULL, kit_token VARCHAR(255) NOT NULL, crm_url VARCHAR(255) NOT NULL, crm_key VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, connection_date DATETIME NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, fio VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, inn VARCHAR(255) NOT NULL, kpp VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, region VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, building VARCHAR(255) NOT NULL, house VARCHAR(255) DEFAULT NULL, flat VARCHAR(255) DEFAULT NULL, like_cus TINYINT(1) DEFAULT \'1\' NOT NULL, f_country VARCHAR(255) DEFAULT NULL, f_city VARCHAR(255) DEFAULT NULL, f_region VARCHAR(255) DEFAULT NULL, f_street VARCHAR(255) DEFAULT NULL, f_building VARCHAR(255) DEFAULT NULL, f_house VARCHAR(255) DEFAULT NULL, f_flat VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kit_parsel (id INT AUTO_INCREMENT NOT NULL, clientId INT NOT NULL, orderId VARCHAR(255) NOT NULL, trackId VARCHAR(255) NOT NULL, site VARCHAR(255) NOT NULL, isClose TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE connection');
        $this->addSql('DROP TABLE parsel');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE connection (id INT AUTO_INCREMENT NOT NULL, kit_token VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, crm_url VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, crm_key VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) NOT NULL, connection_date DATETIME NOT NULL, type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, fio VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, inn VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, kpp VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, country VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, city VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, region VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, street VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, building VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, house VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, flat VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, like_cus TINYINT(1) DEFAULT \'1\' NOT NULL, f_country VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_city VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_region VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_street VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_building VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_house VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, f_flat VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parsel (id INT AUTO_INCREMENT NOT NULL, clientId INT NOT NULL, orderId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, trackId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, isClose TINYINT(1) NOT NULL, site VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE kit_connection');
        $this->addSql('DROP TABLE kit_parsel');
    }
}

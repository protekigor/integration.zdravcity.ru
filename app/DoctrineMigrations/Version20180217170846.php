<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180217170846 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kit_connection DROP name, DROP fio, DROP phone, DROP inn, DROP kpp, DROP country, DROP city, DROP region, DROP street, DROP building, DROP house, DROP flat, DROP like_cus, DROP f_country, DROP f_city, DROP f_region, DROP f_street, DROP f_building, DROP f_house, DROP f_flat');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kit_connection ADD name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD fio VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD phone VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD inn VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD kpp VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD country VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD city VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD region VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD street VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD building VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD house VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD flat VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD like_cus TINYINT(1) DEFAULT \'1\' NOT NULL, ADD f_country VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_city VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_region VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_street VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_building VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_house VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD f_flat VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}

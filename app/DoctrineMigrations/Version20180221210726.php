<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180221210726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pec_connection (id INT AUTO_INCREMENT NOT NULL, hash VARCHAR(255) NOT NULL, pec_login VARCHAR(255) NOT NULL, pecKey VARCHAR(255) NOT NULL, crm_url VARCHAR(255) NOT NULL, crm_key VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, connection_date DATETIME NOT NULL, inn VARCHAR(255) DEFAULT NULL, series VARCHAR(255) NOT NULL, number VARCHAR(255) NOT NULL, created_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pec_parsel (id INT AUTO_INCREMENT NOT NULL, clientId INT NOT NULL, orderId VARCHAR(255) NOT NULL, trackId VARCHAR(255) NOT NULL, site VARCHAR(255) NOT NULL, isClose TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE pec_connection');
        $this->addSql('DROP TABLE pec_parsel');
    }
}

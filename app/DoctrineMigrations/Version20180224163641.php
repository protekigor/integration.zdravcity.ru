<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180224163641 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pec_connection CHANGE inn inn VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pec_parsel ADD cargoCode VARCHAR(255) NOT NULL, ADD barcode VARCHAR(255) NOT NULL, CHANGE trackid documentId VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pec_connection CHANGE inn inn VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE pec_parsel ADD trackId VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP documentId, DROP cargoCode, DROP barcode');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180624212346 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE goods_boxes (id INT AUTO_INCREMENT NOT NULL, boxes VARCHAR(255) NOT NULL, clientId INT DEFAULT NULL, INDEX IDX_CE74B8FEEA1CE9BE (clientId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE goods_item_cancel_status (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, clientId INT DEFAULT NULL, UNIQUE INDEX UNIQ_9B493C20EA1CE9BE (clientId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE goods_shipments (id INT AUTO_INCREMENT NOT NULL, shipmentId INT NOT NULL, items VARCHAR(255) NOT NULL, confirmed TINYINT(1) DEFAULT \'0\' NOT NULL, packed TINYINT(1) DEFAULT \'0\' NOT NULL, clientId INT DEFAULT NULL, INDEX IDX_8BCD4D90EA1CE9BE (clientId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE goods_status_confirm (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, clientId INT DEFAULT NULL, INDEX IDX_82D8504AEA1CE9BE (clientId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE goods_boxes ADD CONSTRAINT FK_CE74B8FEEA1CE9BE FOREIGN KEY (clientId) REFERENCES goods_connection (id)');
        $this->addSql('ALTER TABLE goods_item_cancel_status ADD CONSTRAINT FK_9B493C20EA1CE9BE FOREIGN KEY (clientId) REFERENCES goods_connection (id)');
        $this->addSql('ALTER TABLE goods_shipments ADD CONSTRAINT FK_8BCD4D90EA1CE9BE FOREIGN KEY (clientId) REFERENCES goods_connection (id)');
        $this->addSql('ALTER TABLE goods_status_confirm ADD CONSTRAINT FK_82D8504AEA1CE9BE FOREIGN KEY (clientId) REFERENCES goods_connection (id)');
        $this->addSql('ALTER TABLE goods_connection ADD merchantId INT DEFAULT 0');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE goods_boxes');
        $this->addSql('DROP TABLE goods_item_cancel_status');
        $this->addSql('DROP TABLE goods_shipments');
        $this->addSql('DROP TABLE goods_status_confirm');
        $this->addSql('ALTER TABLE goods_connection DROP merchantId');
    }
}

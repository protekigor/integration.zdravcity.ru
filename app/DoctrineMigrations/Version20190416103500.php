<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190416103500 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rs_modes (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rs_rules ADD product_id INT DEFAULT NULL, ADD mode_id INT NOT NULL');
        $this->addSql('ALTER TABLE rs_rules ADD CONSTRAINT FK_E4B757314584665A FOREIGN KEY (product_id) REFERENCES rs_products (id)');
        $this->addSql('ALTER TABLE rs_rules ADD CONSTRAINT FK_E4B7573177E5854A FOREIGN KEY (mode_id) REFERENCES rs_modes (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E4B757314584665A ON rs_rules (product_id)');
        $this->addSql('CREATE INDEX IDX_E4B7573177E5854A ON rs_rules (mode_id)');
        $this->addSql('ALTER TABLE rs_products ADD item VARCHAR(255) NOT NULL, CHANGE items recommended_items LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rs_rules DROP FOREIGN KEY FK_E4B7573177E5854A');
        $this->addSql('DROP TABLE rs_modes');
        $this->addSql('ALTER TABLE rs_products DROP item, CHANGE recommended_items items LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE rs_rules DROP FOREIGN KEY FK_E4B757314584665A');
        $this->addSql('DROP INDEX UNIQ_E4B757314584665A ON rs_rules');
        $this->addSql('DROP INDEX IDX_E4B7573177E5854A ON rs_rules');
        $this->addSql('ALTER TABLE rs_rules DROP product_id, DROP mode_id');
    }
}

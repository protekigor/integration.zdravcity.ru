<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190918085248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE points_dictionary (id INT AUTO_INCREMENT NOT NULL, address LONGTEXT NOT NULL, amount_to VARCHAR(255) NOT NULL, building_type VARCHAR(255) NOT NULL, card VARCHAR(255) NOT NULL, cash VARCHAR(255) NOT NULL, citi_id INT NOT NULL, citi_name VARCHAR(255) NOT NULL, citi_owner_id INT NOT NULL, closing_comment VARCHAR(255) NOT NULL, closing_date_from VARCHAR(255) NOT NULL, closing_date_to VARCHAR(255) NOT NULL, comment VARCHAR(255) NOT NULL, country_iso VARCHAR(255) NOT NULL, country_name VARCHAR(255) NOT NULL, fitting TINYINT(1) NOT NULL, house VARCHAR(255) NOT NULL, point_id INT NOT NULL, in_description VARCHAR(255) NOT NULL, in_door_place VARCHAR(255) NOT NULL, latitude DOUBLE PRECISION NOT NULL, location_type INT NOT NULL, longitude INT NOT NULL, map_allowed TINYINT(1) NOT NULL, max_box_size VARCHAR(255) NOT NULL, max_size VARCHAR(255) NOT NULL, max_weight VARCHAR(255) NOT NULL, metro VARCHAR(255) NOT NULL, metro_array LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', moving_comment VARCHAR(255) NOT NULL, moving_date_from VARCHAR(255) NOT NULL, moving_date_to VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, number VARCHAR(255) NOT NULL, opening TINYINT(1) NOT NULL, out_description VARCHAR(255) NOT NULL, owner_id INT NOT NULL, owner_name VARCHAR(255) NOT NULL, pay_pass_available TINYINT(1) NOT NULL, post_code VARCHAR(255) NOT NULL, region VARCHAR(255) NOT NULL, return_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', returning TINYINT(1) NOT NULL, status INT NOT NULL, street VARCHAR(255) NOT NULL, type_title VARCHAR(255) NOT NULL, widget_allowed TINYINT(1) NOT NULL, work_hourly TINYINT(1) NOT NULL, work_time LONGTEXT NOT NULL, work_time_sms VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE points_dictionary');
    }
}

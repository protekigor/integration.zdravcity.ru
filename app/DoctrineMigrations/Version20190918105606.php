<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190918105606 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE points_dictionary CHANGE address address LONGTEXT DEFAULT NULL, CHANGE amount_to amount_to VARCHAR(255) DEFAULT NULL, CHANGE building_type building_type VARCHAR(255) DEFAULT NULL, CHANGE card card VARCHAR(255) DEFAULT NULL, CHANGE cash cash VARCHAR(255) DEFAULT NULL, CHANGE citi_id citi_id INT DEFAULT NULL, CHANGE citi_name citi_name VARCHAR(255) DEFAULT NULL, CHANGE citi_owner_id citi_owner_id INT DEFAULT NULL, CHANGE closing_comment closing_comment VARCHAR(255) DEFAULT NULL, CHANGE closing_date_from closing_date_from VARCHAR(255) DEFAULT NULL, CHANGE closing_date_to closing_date_to VARCHAR(255) DEFAULT NULL, CHANGE comment comment VARCHAR(255) DEFAULT NULL, CHANGE country_iso country_iso VARCHAR(255) DEFAULT NULL, CHANGE country_name country_name VARCHAR(255) DEFAULT NULL, CHANGE house house VARCHAR(255) DEFAULT NULL, CHANGE point_id point_id INT DEFAULT NULL, CHANGE in_description in_description VARCHAR(255) DEFAULT NULL, CHANGE in_door_place in_door_place VARCHAR(255) DEFAULT NULL, CHANGE latitude latitude DOUBLE PRECISION DEFAULT NULL, CHANGE location_type location_type INT DEFAULT NULL, CHANGE longitude longitude INT DEFAULT NULL, CHANGE max_box_size max_box_size VARCHAR(255) DEFAULT NULL, CHANGE max_size max_size VARCHAR(255) DEFAULT NULL, CHANGE max_weight max_weight VARCHAR(255) DEFAULT NULL, CHANGE metro metro VARCHAR(255) DEFAULT NULL, CHANGE moving_comment moving_comment VARCHAR(255) DEFAULT NULL, CHANGE moving_date_from moving_date_from VARCHAR(255) DEFAULT NULL, CHANGE moving_date_to moving_date_to VARCHAR(255) DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL, CHANGE number number VARCHAR(255) DEFAULT NULL, CHANGE out_description out_description VARCHAR(255) DEFAULT NULL, CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE owner_name owner_name VARCHAR(255) DEFAULT NULL, CHANGE post_code post_code VARCHAR(255) DEFAULT NULL, CHANGE region region VARCHAR(255) DEFAULT NULL, CHANGE status status INT DEFAULT NULL, CHANGE street street VARCHAR(255) DEFAULT NULL, CHANGE type_title type_title VARCHAR(255) DEFAULT NULL, CHANGE work_time work_time LONGTEXT DEFAULT NULL, CHANGE work_time_sms work_time_sms VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE points_dictionary CHANGE address address LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE amount_to amount_to VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE building_type building_type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE card card VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE cash cash VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE citi_id citi_id INT NOT NULL, CHANGE citi_name citi_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE citi_owner_id citi_owner_id INT NOT NULL, CHANGE closing_comment closing_comment VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE closing_date_from closing_date_from VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE closing_date_to closing_date_to VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE comment comment VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE country_iso country_iso VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE country_name country_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE house house VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE point_id point_id INT NOT NULL, CHANGE in_description in_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE in_door_place in_door_place VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE latitude latitude DOUBLE PRECISION NOT NULL, CHANGE location_type location_type INT NOT NULL, CHANGE longitude longitude INT NOT NULL, CHANGE max_box_size max_box_size VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE max_size max_size VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE max_weight max_weight VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE metro metro VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE moving_comment moving_comment VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE moving_date_from moving_date_from VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE moving_date_to moving_date_to VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE name name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE number number VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE out_description out_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE owner_id owner_id INT NOT NULL, CHANGE owner_name owner_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE post_code post_code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE region region VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE status status INT NOT NULL, CHANGE street street VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE type_title type_title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE work_time work_time LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE work_time_sms work_time_sms VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}

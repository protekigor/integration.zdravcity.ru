<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190322083816 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rules ADD CONSTRAINT FK_899A993C35D8B527 FOREIGN KEY (rule_type_id) REFERENCES rule_types (id)');
        $this->addSql('ALTER TABLE rules ADD CONSTRAINT FK_899A993CDD03F01 FOREIGN KEY (connection_id) REFERENCES connection (id)');
        $this->addSql('CREATE INDEX IDX_899A993C35D8B527 ON rules (rule_type_id)');
        $this->addSql('CREATE INDEX IDX_899A993CDD03F01 ON rules (connection_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rules DROP FOREIGN KEY FK_899A993C35D8B527');
        $this->addSql('ALTER TABLE rules DROP FOREIGN KEY FK_899A993CDD03F01');
        $this->addSql('DROP INDEX IDX_899A993C35D8B527 ON rules');
        $this->addSql('DROP INDEX IDX_899A993CDD03F01 ON rules');
    }
}

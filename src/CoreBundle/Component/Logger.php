<?php

namespace CoreBundle\Component;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Monolog\Logger as MonologLogger;
use CoreBundle\Exception\MonologGetLevelException;

/**
 * Class Logger
 *
 * @package Core\AutomateBundle\Component
 */
class Logger
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var int
     */
    private $startTime;

    /**
     * @var string
     */
    private $prevChannelMessage;

    /**
     * @param null $container
     *
     * @return \CoreBundle\Component\Logger
     * @throws \Exception
     */
    public static function getInstance($container = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($container);
        }

        return self::$instance;
    }

    /**
     * Logger constructor.
     * @param null $container
     * @throws \Exception
     */
    final private function __construct($container = null)
    {
        if (is_null($this->container)) {
            if (is_null($container)) {
                throw new \Exception('Need container!');
            } else {
                $this->container = $container;
            }
        }

        $this->startTime = microtime(true);
    }

    final private function __clone()
    {
    }

    final private function __sleep()
    {
    }

    final private function __wakeup()
    {
    }

    /**
     * @param string $channel
     * @param string $message
     *
     * @return \CoreBundle\Component\Logger
     */
    public function log(string $channel, string $message): self
    {
        unset($this->prevChannelMessage);

        try {
            /** @var \Monolog\Logger $monologService */
            $monologService = $this->container->get("monolog.logger.$channel");

            try {
                $monologService->addRecord($this->getErrorTypeCall($monologService), $message);
            } catch (MonologGetLevelException $ex) {
                $this->reserveLog($ex->getMessage());
            }
        } catch (\Throwable $ex) {
            $this->reserveLog($ex->getMessage());
        }

        $this->prevChannelMessage = $message;

        return $this;
    }

    /**
     * Duplicate message to other log channel
     *
     * @param string $channel
     *
     * @return \CoreBundle\Component\Logger
     */
    public function duplicate(string $channel): self
    {
        if (!empty($this->prevChannelMessage)) {
            try {
                /** @var \Monolog\Logger $monologService */
                $monologService = $this->container->get("monolog.logger.$channel");

                try {
                    $monologService->addRecord($this->getErrorTypeCall($monologService), $this->prevChannelMessage);
                } catch (MonologGetLevelException $ex) {
                    $this->reserveLog($ex->getMessage());
                }
            } catch (\Throwable $ex) {
                $this->reserveLog($ex->getMessage());
            }
        }

        return $this;
    }

    /**
     * @param \Monolog\Logger $stream
     *
     * @return int
     * @throws \CoreBundle\Exception\MonologGetLevelException
     */
    private function getErrorTypeCall(MonologLogger $stream): int
    {
        foreach ($stream->getHandlers() as $handler) {
            if (method_exists($handler, 'getUrl')
                && stripos($handler->getUrl(), $stream->getName()) !== false
                && method_exists($handler, 'getLevel')
            ) {
                return $handler->getLevel();
            }

            if (method_exists($handler, 'getLevel')) {
                return $handler->getLevel();
            }
        }

        throw new MonologGetLevelException(
            "Can't identify the level for the current logger service ({$stream->getName()})"
        );
    }

    /**
     * @param string $message
     *
     * @return bool
     */
    public function reserveLog(string $message): bool
    {
        $this->container->get("monolog.logger.reserved")->addError($message);

        return true;
    }
}

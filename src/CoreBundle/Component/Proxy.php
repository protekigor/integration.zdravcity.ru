<?php

namespace CoreBundle\Component;

use CoreBundle\Model\Client;
use CoreBundle\Model\LoggerDirectory;
use CoreBundle\Component\Logger;

/**
 * Class Proxy
 *
 * @package CoreBundle\Component
 */
class Proxy
{
    /**
     * @var object
     */
    private $requestClass;

    /**
     * @var ?string
     */
    private $responseHandler = null;

    /**
     * @param string $name
     *
     * @return $this
     * @throws \Exception
     */
    public function __get(string $name)
    {
        if (!property_exists($this->requestClass, $name)) {
            return $this;
        }

        try {
            $this->requestClass = $this->requestClass->{$name};
        } catch (\Throwable $exception) {
            try {
                $exceptionType = (new \ReflectionClass($exception))->getShortName();
            } catch (\ReflectionException $exceptioRef) {
                $exceptionType = 'UndefindException';
            }

            Logger::getInstance()->log(
                (
                !empty(LoggerDirectory::get('current')) ?
                    (LoggerDirectory::get('current') . '_') :
                    ''
                ) . 'proxy',
                sprintf(
                    "[%d][method: '%s'] error: '%s'",
                    Client::get('current') ?? 0,
                    $exceptionType,
                    $exception->getMessage()
                )
            );
        }

        return $this;
    }

    /**
     * Proxy constructor.
     *
     * @param string $requestClassName
     * @param array  $params
     *
     * @throws \Exception
     */
    public function __construct(string $requestClassName, array $params)
    {
        try {
            $this->requestClass = new $requestClassName(...$params);
        } catch (\Throwable $exception) {
            try {
                $exceptionType = (new \ReflectionClass($exception))->getShortName();
            } catch (\ReflectionException $exception) {
                $exceptionType = 'UndefindException';
            }

            Logger::getInstance()->log(
                (
                !empty(LoggerDirectory::get('current')) ?
                    (LoggerDirectory::get('current') . '_') :
                    ''
                ) . 'proxy',
                sprintf(
                    "[%d][method: '%s'] error: '%s'",
                    Client::get('current') ?? 0,
                    $exceptionType,
                    $exception->getMessage()
                )
            );

            $this->fatalError = true;
        }

        return $this;
    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return object
     * @throws \Exception
     */
    public function __call(string $method, array $arguments)
    {
        try {
            $response = call_user_func_array(
                [$this->requestClass, $method],
                $arguments
            );
            
            if (!is_null($this->responseHandler)) {
                $handler = new $this->responseHandler;
                return $handler($response);
            } else {
                return $response;
            }
        } catch (\Throwable $exception) {
            try {
                $exceptionType = (new \ReflectionClass($exception))->getShortName();
            } catch (\ReflectionException $exception) {
                $exceptionType = 'UndefindException';
            }

            Logger::getInstance()->log(
                (
                    !empty(LoggerDirectory::get('current')) ?
                        (LoggerDirectory::get('current') . '_') :
                        ''
                ) . 'proxy',
                sprintf(
                    "[%d][method: '%s'] error: '%s'",
                    Client::get('current') ?? 0,
                    $exceptionType,
                    $exception->getMessage()
                )
            );

            return ["errors" => true, "errorType" => $exceptionType, "message" => $exception->getMessage()];
        }
    }

    /**
     * @param string $handlerClass
     */
    public function setResponseHandler(string $handlerClass)
    {
        $this->responseHandler = $handlerClass;
    }
}

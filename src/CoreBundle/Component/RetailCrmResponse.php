<?php

namespace CoreBundle\Component;

use RetailCrm\Response\ApiResponse;
use CoreBundle\Exception\ResponseErrorException;

/**
 * Class RetailCrmResponse
 *
 * @package CoreBundle\Component
 */
class RetailCrmResponse
{
    /**
     * @param \RetailCrm\Response\ApiResponse $response
     *
     * @return array
     * @throws \CoreBundle\Exception\ResponseErrorException
     */
    public function __invoke(ApiResponse $response): array
    {
        if (!$response->isSuccessful()) {
            try {
                $errors = '';

                foreach ($response['errors'] as $key => $value) {
                    $errors .= sprintf(
                        '[%s] -> %s | ',
                        $key,
                        $value
                    );
                }

                unset($key, $value);
                $errors = trim($errors, ' | ');
            } catch (\Throwable $exception) {
                $errors = $response->getErrorMsg();
            }

            $error = is_array($errors) ? implode('; ', $errors) : $errors;
            throw new ResponseErrorException($error);
        }

        return $response->getResponse();
    }
}

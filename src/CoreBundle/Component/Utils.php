<?php

namespace CoreBundle\Component;

/**
 * Class Utils
 *
 * @package Modules\Deliveries\KitBundle\Component
 */
class Utils
{
    /**
     * @param string $fio
     *
     * @return array
     */
    public static function explodeFIO(string $fio): array
    {
        $result = [];
        $fio = trim($fio);
        $parse = explode(" ", $fio);

        switch (count($parse)) {
            case 3:
                $result = [
                    'lastName' => $parse[0],
                    'firstName' => $parse[1],
                    'patronymic' => $parse[2]
                ];

                break;
            case 2:
                $result = [
                    'lastName' => $parse[0],
                    'firstName' => $parse[1]
                ];

                break;
            case 1:
                $result = [
                    'firstName' => $parse[0]
                ];

                break;
        }

        return $result;
    }

    /**
     * @param array $arr
     * @param bool  $trimming
     *
     * @return array
     */
    public static function clearArray(array $arr, bool $trimming = true): array
    {
        if (!is_array($arr)) {
            return $arr;
        }

        $result = [];

        foreach ($arr as $index => $node) {
            $result[ $index ] = (is_array($node))
                ? self::clearArray($node, $trimming)
                : ($trimming ? trim($node) : $node);

            if ($result[ $index ] === ''
                || $result[ $index ] === null
                || count($result[ $index ]) < 1
            ) {
                unset($result[ $index ]);
            }
        }

        return $result;
    }

    /**
     * @param array $array
     *
     * @return string
     */
    public static function arrayToString(array $array): string
    {
        $string = '';

        foreach ($array as $index => $value) {
            $string .= sprintf(
                '[%s => %s]',
                $index,
                (is_array($value) ? self::arrayToString($value) : $value)
            );
        }

        unset($index, $value);

        return $string;
    }

    /**
     * @param array $array
     *
     * @return bool
     */
    public static function isAssoc(array $array): bool
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }
}

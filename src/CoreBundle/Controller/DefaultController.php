<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CoreBundle\Component\Logger;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('CoreBundle:Default:index.html.twig');
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        Logger::getInstance($container);
    }

    /**
     * @Route("/new")
     */
    public function goodsNewAction(Request $request)
    {
        $json = $request->getContent();
        Logger::getInstance()->log('goods_test', "[New]: $json");

        return new Response('OK', 200);
    }

    /**
     * @Route("/labelUpdate")
     */
    public function goodsLabelUpdateAction(Request $request)
    {
        $json = $request->getContent();
        Logger::getInstance()->log('goods_test', "[LabelUpdate]: $json");

        return new Response('OK', 200);
    }

    /**
     * @Route("/cancel")
     */
    public function goodsCancelAction(Request $request)
    {
        $json = $request->getContent();
        Logger::getInstance()->log('goods_test', "[Cancel]: $json");

        return new Response('OK', 200);
    }
}

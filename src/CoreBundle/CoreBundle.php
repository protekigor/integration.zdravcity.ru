<?php

namespace CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use CoreBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;

/**
 * Class CoreBundle
 *
 * @package CoreBundle
 */
class CoreBundle extends Bundle
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceCompilerPass());
    }
}

<?php

namespace CoreBundle\Exception;

use CoreBundle\Exception\Traits\ExceptionTrait;

/**
 * Class MonologGetLevelException
 *
 * @package CoreBundle\Exception
 */
class MonologGetLevelException extends \Exception
{
    use ExceptionTrait;
}

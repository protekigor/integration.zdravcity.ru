<?php

namespace CoreBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreBundle\Listener\Traits\ListenerTrait;
use CoreBundle\Component\Logger;

/**
 * Class HttpExceptionListener
 *
 * @package CoreBundle\Listener
 */
class HttpExceptionListener extends Controller
{
    use ListenerTrait;

    /* @var ContainerInterface $container */
    public $container;

    private static $bundles = [];

    /**
     * @param GetResponseEvent $event
     *
     * @throws \Exception
     */
    public function onStart(GetResponseEvent $event)
    {
        Logger::getInstance($this->container);
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
     *
     * @throws \Exception
     */
    public function onError(GetResponseForExceptionEvent $event)
    {
        if ($this->container->get('kernel')->getEnvironment() === 'prod') {
            Logger::getInstance($this->container);
            $this->getFullStringErrorMessage($event);
            $exception = $event->getException();
            $response = new Response();

            if ($exception instanceof HttpExceptionInterface) {
                $code = $exception->getStatusCode();

                $content = $this->renderView(
                    'CoreBundle:Modules:error.html.twig',
                    [
                        'error' => $exception->getMessage(),
                        'referer' => '/'
                    ]
                );

                $response->headers->replace($exception->getHeaders());
            } else {
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                $content = $exception->getMessage();
            }

            $response->setStatusCode($code);
            $response->setContent($content);
            $event->setResponse($response);
        }
    }
}

<?php

namespace CoreBundle\Listener\Traits;

use Symfony\Component\Console\Event\ConsoleEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use CoreBundle\Model\Client;
use CoreBundle\Component\Logger;
use CoreBundle\Model\Bundle;

/**
 * Class ListenerTrait
 *
 * @package CoreBundle\Listener\Traits
 */
trait ListenerTrait
{
    /**
     * @param object $event
     *
     * @throws \Exception
     */
    public function getFullStringErrorMessage($event)
    {
        /** @var \Throwable $exception */
        if (method_exists($event, 'getError')) {
            $exception = $event->getError();
        } else {
            $exception = $event->getException();
        }

        try {
            $appException = (new \ReflectionClass($exception))->getShortName();
        } catch (\ReflectionException $ex) {
            $appException = 'UndefinedException';
        }

        $traces = self::getTrace($exception);

        try {
            $currenClientId = Client::get('current');
        } catch (\Throwable $ex) {
            $currenClientId = 0;
        }

        if ($event instanceof ConsoleEvent) {
            $channel = 'console_errors';
        } else {
            $channel = 'controller_errors';
        }

        $bundleName = self::getBundleName($event, $exception);

        while (!is_null($exception)) {
            Logger::getInstance()->log(
                $channel,
                sprintf(
                    "[%s][%d][Symfony: %s][%s][line = %d][message \"%s\"]%s",
                    $bundleName,
                    $currenClientId,
                    $appException,
                    preg_replace("#^.+/([^/]+)$#", "$1", $exception->getFile()),
                    $exception->getLine(),
                    $exception->getMessage(),
                    $traces
                )
            );

            $exception = $exception->getPrevious();
            $traces = '';
        }
    }

    /**
     * @param \Throwable $exception
     *
     * @return string
     */
    private static function getTrace(\Throwable &$exception): string
    {
        $traces = '';
        $startWriteTrace = false;
        $allTraces = array_reverse($exception->getTrace());

        foreach ($allTraces as $key => $trace) {
            if ($trace['function'] == 'execute') {
                $startWriteTrace = true;
            }

            if ($startWriteTrace) {
                $path = $trace['class'] ?? $trace['file'];
                $nextTrace = $allTraces[$key + 1] ?? $trace;

                $line = $nextTrace['line'] ??
                    (
                    !empty($trace['line']) ?
                        "({$trace['line']})" :
                        $exception->getLine()
                    );

                $traceClass = !empty($trace['class']) ?
                    preg_replace("#^.+\\\([^\\\]+)$#", "$1", $path) :
                    preg_replace("#^.+/([^/]+)$#", "$1", $path);

                if ($nextTrace == $trace) {
                    $traces .= "\"{$trace['function']}\"";
                } else {
                    $traces .= "\"{$trace['function']}\" in {$traceClass}({$line}) -> ";
                }
            }
        }

        return sprintf("[trace: %s]", trim($traces, ' -> '));
    }

    /**
     * @param                 $event
     * @param \Throwable|null $exception
     *
     * @return string
     */
    private static function getBundleName(&$event, \Throwable $exception = null): string
    {
        $class = self::class;
        if ($event instanceof ConsoleEvent) {
            $class = get_class($event->getCommand());
        } else {
            $class = str_replace(
                '/',
                '\\',
                preg_replace("#^.+/src/(.+)\.php$#", "$1", $exception->getFile())
            );
        }
        try {
            $reflector = new \ReflectionClass($class);
        } catch (\Throwable $ex) {
            $reflector = null;
        }
        if (is_null($reflector)) {
            return 'UndefinedBundle';
        } else {
            return str_replace(
                '\\',
                '',
                ($p1 = strpos($ns = $reflector->getNamespaceName(), '\\')) === false ?
                    $ns :
                    substr(
                        $ns,
                        0,
                        ($p2 = strpos($ns, '\\', $p1 + 1)) === false ? strlen($ns) : $p2
                    )
            );
        }
    }
}

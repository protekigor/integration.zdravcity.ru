<?php

namespace CoreBundle\Model;

use CoreBundle\Model\Traits\ModelProvider;
use CoreBundle\Model\Model as ModelInterface;

/**
 * Class Client
 *
 * @package CoreBundle\Model
 */
class Client implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];
}
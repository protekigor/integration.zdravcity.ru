<?php

namespace CoreBundle\Model;

use CoreBundle\Model\Traits\ModelProvider;
use CoreBundle\Model\Model as ModelInterface;

class LoggerDirectory implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];
}
<?php

namespace CoreBundle\Model;

/**
 * Interface Model
 *
 * @package CoreBundle\Model
 */
interface Model
{
    /**
     * Set new value with key
     *
     * @param string $key
     * @param mixed $value
     *
     * @return void
     */
    public static function set(string $key, $value);

    /**
     * Get value by a specific key
     *
     * @param string $key
     *
     * @return mixed
     */
    public static function get(string $key);

    /**
     * Remove value by a specific key
     *
     * @param string $key
     *
     * @return void
     */
    public static function removeValue(string $key);

    /**
     * Return count elements
     *
     * @return int
     */
    public static function getCount();
}

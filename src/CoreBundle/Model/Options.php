<?php

namespace CoreBundle\Model;

use CoreBundle\Model\Traits\ModelProvider;
use CoreBundle\Model\Model as ModelInterface;
use CoreBundle\Component\Utils;

/**
 * Class Options
 *
 * @package CoreBundle\Model
 */
class Options implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];

    /**
     * Проверяет, есть ли значение в справочнике
     *
     * @param mixed $value
     * @return bool
     */
    final public static function issetValue($value)
    {
        return in_array($value, self::$data);
    }

    /**
     * Добавляет в справочник набор параметров
     *
     * @param array $options
     * @throws \InvalidArgumentException
     * @return void
     */
    final public static function configure(array $options)
    {
        if (Utils::isAssoc($options)) {
            throw new \InvalidArgumentException('The configuration array must be associative!');
        }
        self::$data = array_merge(self::$data, $options);
    }
}

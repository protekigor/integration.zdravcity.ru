$('#nPanel a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
});

$('#hide_person_data').click(function () {
    if ($(this).prop('checked')) {
        $('#contact_person_data').stop(true).hide(175);
    } else {
        $('#contact_person_data').stop(true).show(175);
    }
});
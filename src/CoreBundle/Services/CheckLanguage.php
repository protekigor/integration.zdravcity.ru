<?php

namespace CoreBundle\Services;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;

class CheckLanguage
{
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public function checkAndSetDefault(Request $request, string $default, array $optionLanguages = [])
    {
        $language = $request->getPreferredLanguage($optionLanguages);
        if (in_array($language, $optionLanguages)) {
            $this->translator->setLocale($language);
        } else {
            $this->translator->setLocale($default);
        }
    }
}
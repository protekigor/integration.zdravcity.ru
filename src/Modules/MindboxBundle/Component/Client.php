<?php

namespace Modules\MindboxBundle\Component;

use CoreBundle\Component\Proxy;

/**
 * Class Client
 *
 * @package Modules\MindboxBundle\Component
 */
class Client
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Client constructor.
     *
     * @param string $apiKey
     *
     * @return self
     * @throws \Exception
     */
    public function __construct(string $apiKey)
    {
        $this->request = new Proxy(Request::class, [$apiKey]);

        return $this;
    }

    /**
     * @param string    $controller
     * @param string    $action
     * @param array     $options
     *
     * @return array
     */
    public function get(string $action, array $options = []): array
    {
        $params['action'] = $action;
        $params['data'] = $options;

        return $this->request->get($params);
    }

    public function post(string $action, array $options = []): array
    {
        $params['action'] = $action;
        $params['data'] = $options;

        return $this->request->post($params);
    }
}

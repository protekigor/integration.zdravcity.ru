<?php

namespace Modules\MindboxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CoreBundle\Component\Logger;
use CoreBundle\Model\Client;
use CoreBundle\Model\LoggerDirectory;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Persistence\ObjectManager;
use CoreBundle\Services\CheckLanguage;

/**
 * Class BaseController
 *
 * @package Modules\MindboxBundle\Controller
 */
abstract class BaseController extends Controller
{
    /**
     * @var object
     */
    protected $connection;

    /**
     * @var object
     */
    protected $mindbox;

    /**
     * @var string
     */
    protected $primaryField;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var string
     */
    protected $session_id;

    protected $languageChecker;

    /**
     * BaseController constructor.
     *
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestService
     * @param \Doctrine\Common\Persistence\ObjectManager     $entityManager
     * @param \CoreBundle\Services\CheckLanguage             $languageChecker
     *
     * @throws \Exception
     */
    public function __construct(
        RequestStack $requestService,
        ObjectManager $entityManager,
        CheckLanguage $languageChecker
    ) {
        $this->em = $entityManager;
        $this->languageChecker = $languageChecker;
        $request = $requestService->getCurrentRequest();

        LoggerDirectory::set('current', 'mindbox');
        $this->channel = 'mindbox_callback';

        $languages = ['en', 'ru'];
        $defaultLanguage = 'ru';
        $this->languageChecker->checkAndSetDefault($request, $defaultLanguage, $languages);

        try {
            $this->mindbox = $this->setConnection();
        } catch (\Exception $exception) {
            $this->mindbox = false;

            Logger::getInstance()->log(
                $this->channel,
                'Не удалось инициализировать mindbox-api-клиент.'
            );
        }

//        $this->primaryField = $request->get($this->getPrimaryField());
//        $connectionClass = $this->getConnectionClass();
//
//        $this->connection = $this->em->getRepository($connectionClass)
//            ->findOneBy(['hash' => $this->primaryField]);
//
//        if (is_null($this->connection)) {
//            $this->connection = new $connectionClass;
//        } else {
//            try {
//                $this->mindbox = $this->setConnection();
//            } catch (\Exception $exception) {
//                $this->mindbox = false;
//
//                Logger::getInstance()->log(
//                    $this->channel,
//                    'Не удалось инициализировать api-клиент.'
//                );
//            }
//        }
//
//        try {
//            $clientId = $this->connection->getId();
//        } catch (\Throwable $exception) {
//            $clientId = 0;
//        }
//
//        Client::set(
//            'current',
//            $clientId
//        );
    }

//    /**
//     * @return string
//     */
//    abstract protected function getPrimaryField(): string;

    /**
     * @return string
     */
    abstract protected function getConnectionClass(): string;

    /**
     * @return string
     */
    protected function getIdentificationField(): string
    {
        return 'id';
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected function constructValue(string $value): string
    {
        return $value;
    }
}

<?php

namespace Modules\MindboxBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use CoreBundle\Component\Logger;
use Modules\MindboxBundle\Entity\Connection;
use Modules\MindboxBundle\Entity\Parsel;
use Modules\MindboxBundle\Component\Client as MindboxClient;
use Modules\MindboxBundle\Entity\City;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class CallbackController
 *
 * @package Modules\MindboxBundle\Controller
 */
class CallbackController extends BaseController
{
    /**
     * Create order action
     *
     * @param Request $request input request
     *
     * @Route("/order", defaults={"_format": "json"})
     * @Method("POST")
     *
     * @return Response
     * @throws \Exception
     */
    public function createOrderAction(Request $request)
    {
        $params = json_encode($request);

        Logger::getInstance()->log(
            'mindbox_callback',
            sprintf(
                '%s',
                var_export($request, true)
            )
        );

        return new JsonResponse($params);
    }

    /**
     * Submit form action
     *
     * @param Request $request input request
     *
     * @Route("/submit", defaults={"_format": "json"})
     * @Method("POST")
     *
     * @return Response
     * @throws \Exception
     */
    public function submitAction(Request $request)
    {
        $params = json_encode($request->getParameters());

        Logger::getInstance()->log(
            'mindbox_callback',
            sprintf(
                '%s',
                var_export($params, true)
            )
        );

        return new JsonResponse($params);
    }

    /**
     * {@inheritdoc}
     */
    protected function getConnectionClass(): string
    {
        return Connection::class;
    }

//    /**
//     * {@inheritdoc}
//     */
//    protected function getPrimaryField(): string
//    {
//        return 'clientId';
//    }

    /**
     * @return \Modules\MindboxBundle\Component\Client
     * @throws \Exception
     */
    protected function setConnection(): MindboxClient
    {
        return new MindboxClient(
            'admin'
        );
    }
}

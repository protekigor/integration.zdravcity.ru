<?php

namespace Modules\MindboxBundle\Exceptions;

/**
 * Class MindboxException
 *
 * @package Modules\MindboxBundle\Exceptions
 */
class MindboxException extends \Exception
{
}

<?php

namespace Modules\SoapBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Modules\SoapBundle\Component\Client as SoapClient;
use Modules\SoapBundle\Entity\SoapProducts;
use Modules\SoapBundle\Entity\Connection;

/**
 * Class SoapCatalogUploadCommand
 *
 * @package Modules\SoapBundle\Command
 */
class SoapCatalogUploadCommand extends ContainerAwareCommand
{
    /**
     * @var SoapClient
     */
    private $soap;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    protected function configure()
    {
        $this
            ->setName('soap:catalog:upload')
            ->setDescription('Загрузка каталога из Soap')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null
     * @throws \Exception
     */

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $executionStartTime = microtime(true);
        $this->entityManager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->soap = new SoapClient('admin', 'admin');

        $this->getItems();

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("This script took $seconds to execute.");

        return 0;
    }

    public function getItems()
    {
        /** @var SoapConnectionRepository $soapConnection */
        $soapConnection = $this->entityManager->getRepository(Connection::class)
            ->findOneBy(
                [
                    'login' => 'admin'
                ]
            );

        $batchSize = 20;
        $sessionId = $this->soap->openSession(
            [
                'theLogin' => $soapConnection->getLogin(),
                'thePass' => $soapConnection->getPass()
            ]
        );

        $soapProducts = $this->soap->obtainEsEima(
            [
                'eima-app-' => [$sessionId->server, '/', 'eima-app.protek.ru'],
                'theSessionId' => $sessionId->statusString,
                'theEZakazXML' => [],
                'theRowTs' => $soapConnection->getTheRowTs(),
                'theRowCount' => 10000
            ]
        );

        if(isset($soapProducts->ezakazXML->item)) {
            $lastRowTs = end($soapProducts->ezakazXML->item);
            $soapConnection->setTheRowTs($lastRowTs->rowTs);
            $this->entityManager->flush();
        }

        if($soapProducts->ezakazXML->totalRowCount > 0) {

            foreach ($soapProducts->ezakazXML->item as $i => $item) {
                $soapProduct = new SoapProducts();
                $soapProduct->setActiveFlag($item->activeFlag);
                $soapProduct->setCvpItemId((isset($item->cvpItemId)) ? $item->cvpItemId : null);
                $soapProduct->setGuidEsId($item->guidEsId);
                $soapProduct->setGuidInstruction($item->guidInstruction);
                $soapProduct->setGuidProducer($item->guidProducer);
                $soapProduct->setItemName($item->itemName);
                $soapProduct->setPrNotRecept(isset($item->prNotRecept) ? $item->prNotRecept : null);
                $soapProduct->setProducerName(isset($item->producerName) ? $item->producerName : null);

                $this->entityManager->persist($soapProduct);
                if (($i % $batchSize) == 0) {
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
            }
            
            $this->entityManager->flush();
            $this->entityManager->clear();

            $this->getItems();
        }
    }
}

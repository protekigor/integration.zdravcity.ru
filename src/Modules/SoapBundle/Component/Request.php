<?php

namespace Modules\SoapBundle\Component;

use CoreBundle\Component\Request as BaseRequest;
use Modules\SoapBundle\Exceptions\SoapException;
use Modules\SoapBundle\Model\Error;

/**
 * Class Request
 *
 * @package Modules\SoapBundle\Component
 */
class Request extends BaseRequest
{
    /**
     * @var string
     */
    private $url = 'http://eima-app.protek.ru/eima/services/kind_services?wsdl';

//    /**
//     * @var string
//     */
//    private $mainUrl = 'http://eima-app.protek.ru/eima/services/kind_services?wsdl';

    /**
     * @var string
     */
    private $exceptionClass = SoapException::class;

    /**
     * Request constructor.
     *
     * @param string $apiLogin
     * @param string $apiKey
     */
    public function __construct(
        string $apiLogin,
        string $apiKey
    ) {
        $this->setBasicAuth($apiLogin, $apiKey);
    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return array|object
     */
    public function __call(string $method, array $arguments)
    {
        return call_user_func_array(
            [$this, 'makeSoapRequest'],
            [$method, $arguments, ['error', 'errorMessage']]
        );
    }

    /**
     * @param string $apiLogin
     * @param string $apiKey
     */
    protected function setBasicAuth(string $apiLogin, string $apiKey)
    {
        $this->apiLogin = $apiLogin;
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return array|object
     * @throws \Exception
     */
    protected function makeSoapRequest(string $method, array $params = [])
    {
        $soap = new \SoapClient(
            $this->url,
            [
                'soap_version'   => 1,
                'trace'      => 1,
                'exceptions' => 1,
                'encoding'   => 'UTF-8',
                'cache_wsdl' => 1
            ]
        );
        
        if(isset($params[0]['eima-app-'])) {
            $soap->__setCookie('eima-app-', $params[0]['eima-app-'][0]);
            unset($params[0]['eima-app-']);
        }

        try {
            $response = $soap->__soapCall($method, $params);
        } catch (\Throwable $exception) {
            throw new $this->exceptionClass("Внутренняя ошибка SOAP-клиента (method '$method'): " . $exception->getMessage());
        }

        if (is_soap_fault($response)) {
            Error::set('lastError', sprintf("Внутренняя ошибка (method '$method'): %s", $response->faultstring));

            throw new $this->exceptionClass(
                "Ошибка SOAP: (faultcode: '{$response->faultcode}', faultstring: '{$response->faultstring}')"
            );
        }

        $resultErrors = null;

        if (!is_null($resultErrors)) {
            Error::set('lastError', $resultErrors);
            throw new $this->exceptionClass($resultErrors . " [$method][$this->url]");
        }
        
        if($method == 'openSession') {
            $cookies = $soap->__getCookies();
            $response->{"server"} = $cookies['eima-app-'][0];
        }

        return $response;
    }
}

<?php

namespace Modules\SoapBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use CoreBundle\Component\Logger;
use Modules\SoapBundle\Entity\Connection;
use Modules\SoapBundle\Entity\Parsel;
use Modules\SoapBundle\Component\Client as SoapClient;
use Modules\SoapBundle\Entity\City;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class CallbackController
 *
 * @package Modules\SoapBundle\Controller
 */
class CallbackController extends BaseController
{
    /**
     * Get orders by customer
     *
     * @param Request $request input request
     *
     * @Route("/getorders", defaults={"_format": "json"})
     * @Method("GET")
     *
     * @return Response
     * @throws \Exception
     */
    public function shipmentPointListAction(Request $request)
    {
        $params = json_encode($request->request->all());

        Logger::getInstance()->log(
            'soap_callback',
            sprintf(
                '%s',
                var_export($params, true)
            )
        );
        $getOrderStatus = $this->soap->getOrderStatus(
            [
                'theSessionId' => $this->session_id,
                'theEZakazXML' => [
                    'endUser' => [
                        'agentId' => 170186
                    ],
                ],
                'webRequestProperties' => [
                    'start' => 0,
                    'limit' => 10,
                ],
            ]
        );

        return new JsonResponse($params);
    }

    /**
     * {@inheritdoc}
     */
    protected function getConnectionClass(): string
    {
        return Connection::class;
    }

//    /**
//     * {@inheritdoc}
//     */
//    protected function getPrimaryField(): string
//    {
//        return 'clientId';
//    }

    /**
     * @return \Modules\SoapBundle\Component\Client
     * @throws \Exception
     */
    protected function setConnection(): SoapClient
    {
        return new SoapClient(
            'admin', 'admin'
        );
    }

}

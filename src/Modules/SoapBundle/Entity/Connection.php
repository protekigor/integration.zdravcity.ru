<?php

namespace Modules\SoapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Connection
 *
 * @ORM\Table(name="soap_connection")
 * @ORM\Entity(repositoryClass="Modules\SoapBundle\Repository\ConnectionRepository")
 */
class Connection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=255)
     */
    private $pass;

    /**
     * @var int
     *
     * @ORM\Column(name="the_row_ts", type="integer")
     */
    private $theRowTs;

    /**
     * @var int
     *
     * @ORM\Column(name="the_row_ts_for_test", type="integer")
     */
    private $theRowTsForTest;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Connection
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set pass
     *
     * @param string $pass
     *
     * @return Connection
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set theRowTs
     *
     * @param integer $theRowTs
     *
     * @return Connection
     */
    public function setTheRowTs($theRowTs)
    {
        $this->theRowTs = $theRowTs;

        return $this;
    }

    /**
     * Get theRowTs
     *
     * @return int
     */
    public function getTheRowTs()
    {
        return $this->theRowTs;
    }

    /**
     * Set theRowTsForTest
     *
     * @param integer $theRowTsForTest
     *
     * @return Connection
     */
    public function setTheRowTsForTest($theRowTsForTest)
    {
        $this->theRowTsForTest = $theRowTsForTest;

        return $this;
    }

    /**
     * Get theRowTsForTest
     *
     * @return int
     */
    public function getTheRowTsForTest()
    {
        return $this->theRowTsForTest;
    }
}


<?php

namespace Modules\SoapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SoapProducts
 *
 * @ORM\Table(name="soap_products")
 * @ORM\Entity(repositoryClass="Modules\SoapBundle\Repository\SoapProductsRepository")
 */
class SoapProducts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="active_flag", type="boolean")
     */
    private $activeFlag;

    /**
     * @var int
     *
     * @ORM\Column(name="cvp_item_id", type="integer", nullable=true)
     */
    private $cvpItemId;

    /**
     * @var string
     *
     * @ORM\Column(name="guid_es_id", type="string", length=255, nullable=true)
     */
    private $guidEsId;

    /**
     * @var string
     *
     * @ORM\Column(name="guid_instruction", type="string", length=255, nullable=true)
     */
    private $guidInstruction;

    /**
     * @var string
     *
     * @ORM\Column(name="guid_producer", type="string", length=255, nullable=true)
     */
    private $guidProducer;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=255, nullable=true)
     */
    private $itemName;

    /**
     * @var string
     *
     * @ORM\Column(name="producer_name", type="string", length=255, nullable=true)
     */
    private $producerName;

    /**
     * @var bool
     *
     * @ORM\Column(name="pr_not_recept", type="boolean", nullable=true)
     */
    private $prNotRecept;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activeFlag
     *
     * @param boolean $activeFlag
     *
     * @return SoapProducts
     */
    public function setActiveFlag($activeFlag)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag
     *
     * @return bool
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set cvpItemId
     *
     * @param integer $cvpItemId
     *
     * @return SoapProducts
     */
    public function setCvpItemId($cvpItemId)
    {
        $this->cvpItemId = $cvpItemId;

        return $this;
    }

    /**
     * Get cvpItemId
     *
     * @return int
     */
    public function getCvpItemId()
    {
        return $this->cvpItemId;
    }

    /**
     * Set guidEsId
     *
     * @param string $guidEsId
     *
     * @return SoapProducts
     */
    public function setGuidEsId($guidEsId)
    {
        $this->guidEsId = $guidEsId;

        return $this;
    }

    /**
     * Get guidEsId
     *
     * @return string
     */
    public function getGuidEsId()
    {
        return $this->guidEsId;
    }

    /**
     * Set guidInstruction
     *
     * @param string $guidInstruction
     *
     * @return SoapProducts
     */
    public function setGuidInstruction($guidInstruction)
    {
        $this->guidInstruction = $guidInstruction;

        return $this;
    }

    /**
     * Get guidInstruction
     *
     * @return string
     */
    public function getGuidInstruction()
    {
        return $this->guidInstruction;
    }

    /**
     * Set guidProducer
     *
     * @param string $guidProducer
     *
     * @return SoapProducts
     */
    public function setGuidProducer($guidProducer)
    {
        $this->guidProducer = $guidProducer;

        return $this;
    }

    /**
     * Get guidProducer
     *
     * @return string
     */
    public function getGuidProducer()
    {
        return $this->guidProducer;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     *
     * @return SoapProducts
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set producerName
     *
     * @param string $producerName
     *
     * @return SoapProducts
     */
    public function setProducerName($producerName)
    {
        $this->producerName = $producerName;

        return $this;
    }

    /**
     * Get producerName
     *
     * @return string
     */
    public function getProducerName()
    {
        return $this->producerName;
    }

    /**
     * @param bool $prNotRecept
     */
    public function setPrNotRecept($prNotRecept) : void
    {
        $this->prNotRecept = $prNotRecept;

    }

    /**
     * @return bool
     */
    public function getPrNotRecept(): bool
    {
        return $this->prNotRecept;
    }

}


<?php

namespace Modules\SoapBundle\Exceptions;

/**
 * Class SoapException
 *
 * @package Modules\SoapBundle\Exceptions
 */
class SoapException extends \Exception
{
}

<?php

namespace Modules\SoapBundle\Model;

use CoreBundle\Model\Traits\ModelProvider;
use CoreBundle\Model\Model as ModelInterface;

class Error implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];

    /**
     * @param string $key
     * @return bool
     */
    public static function isset(string $key): bool
    {
        return !empty(self::$data[$key]);
    }
}
